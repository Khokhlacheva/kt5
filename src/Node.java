
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

//https://beginnersbook.com/2013/12/java-string-substring-method-example/
//https://www.geeksforgeeks.org/for-each-loop-in-java/
//https://www.javatpoint.com/java-arraylist
//https://howtodoinjava.com/java8/java-8-join-string-array-example/
//https://stackoverflow.com/questions/48605130/how-to-convert-listobject-into-comma-separated-string
//https://stackoverflow.com/questions/53122389/find-last-instance-of-recursive-call

//https://stackoverflow.com/questions/139076/how-to-pretty-print-xml-from-java
//https://stackoverflow.com/questions/997482/does-java-support-default-parameter-values
//https://stackoverflow.com/questions/33281826/java-equivalent-to-pythons-str-format

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }


   public static int isNodes(String s){
      int comma = 0;
      int count = 0;
      for (int i = 0; i < s.length(); i++) {
         if(s.charAt(i) == ')'){
            count--;
         }
         if(s.charAt(i) == '('){
            count++;
         }
         if(s.charAt(i) == ',' && count == 0){
            return i;
         }
      }
      return comma;
   }



   public static Node parsePostfix (String s) {
      boolean x_code = s.charAt(0) == '?' ? true : false;
      if (x_code){
         s = s.substring(1);
      }
      String name = s;
      Node firstChild = null;//new Node(null,null,null);
      Node nextSibling = null; // new Node(null,null,null);
      List<Node> nodes = new ArrayList<>();
      int start = s.indexOf('(');
      int end = s.lastIndexOf(')') + 1;
      if(start == 0 && end == s.length()){
         String m = "String " + s + " has too many brackets";
         throw new RuntimeException(m);
      }
      int isComma = isNodes(s);
      if(isComma != 0){
         Node x_node = parsePostfix("?" + s.substring(0, isComma));
         name = x_node.name;
         firstChild = x_node.firstChild;
         nextSibling = parsePostfix("?" + s.substring(isComma + 1));
      }
      else if (start != -1) {     //&& Comma(s) != -2
         name = s.replace(s.substring(start, end), "");
         String l = s.substring(start + 1, end - 1);
         firstChild = parsePostfix("?" + l);
      }
      else {
         if (s.contains(" ") || s.contains(",") || s.equals("") || s.equals("\t")) {
            String m = "String " + s + " contain restricted chars";
            throw new RuntimeException();
         }
      }
      if(!x_code && nextSibling != null){
         String m = "String " + s +  " has no brackets";
         throw new RuntimeException();
      }
      return new Node(name, firstChild, nextSibling); // TODO!!! return the root
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      if (firstChild == null && nextSibling == null){
         b.append(name);
      }
      if (firstChild != null && nextSibling == null){
         b.append(name +  "(" + firstChild.leftParentheticRepresentation() + ")");
      }
      if (firstChild != null && nextSibling != null){
         b.append(name +  "(" + firstChild.leftParentheticRepresentation() + ")" +
                 "," + nextSibling.leftParentheticRepresentation());
      }
      if (firstChild == null && nextSibling != null){
         b.append(name + "," + nextSibling.leftParentheticRepresentation());
      }
      return b.toString();
   }

   public String pseudoXmlRepresentation(){
      return pseudoXmlRepresentation(1);
   }
   public String pseudoXmlRepresentation(int count){
      StringBuffer buffer = new StringBuffer();
      if (firstChild == null && nextSibling == null){
         buffer.append("<L" + count + ">" + name + "</L" + count + ">");
      }
      if (firstChild != null && nextSibling == null){
         buffer.append("<L" + count + ">" + name +
                 firstChild.pseudoXmlRepresentation(count + 1)  + "</L" + count + ">");
      }
      if (firstChild != null && nextSibling != null){
         buffer.append("<L" + count + ">" + name +
                 firstChild.pseudoXmlRepresentation(count + 1) + "</L" + count + ">" +
                 nextSibling.pseudoXmlRepresentation(count));
      }
      if (firstChild == null && nextSibling != null){
         buffer.append("<L" + count + ">" + name  + "</L" + count + ">" +
                 nextSibling.pseudoXmlRepresentation(count));
      }
      return buffer.toString();
   }

   public static String prettyFormat(String input, int indent) {
      try {
         Source xmlInput = new StreamSource(new StringReader(input));
         StringWriter stringWriter = new StringWriter();
         StreamResult xmlOutput = new StreamResult(stringWriter);
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         transformerFactory.setAttribute("indent-number", indent);
         Transformer transformer = transformerFactory.newTransformer();
         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
         transformer.transform(xmlInput, xmlOutput);
         return xmlOutput.getWriter().toString();
      } catch (Exception e) {
         throw new RuntimeException(e); // simple exception handling, please review it
      }
   }

   public static String prettyFormat(String input) {
      return prettyFormat(input, 2);
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      s = "A(B(C),D(E))";
      s = "(((2,1)-,4)*,(69,3)/)+";  //  "+(*(-(2,1),4),/(69,3))"
      s = "((1,(2)3,4)5)6"; // "6(5(1,3(2),4))" (1,(2)3,4)5     1,(2)3,4
      //s = "((C)B,(E,F)D,G)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)

      System.out.println(prettyFormat("<root><child>aaa</child><child/></root>"));
      String x = t.pseudoXmlRepresentation();
      System.out.println(x);
      System.out.println(prettyFormat(x));
   }
}

